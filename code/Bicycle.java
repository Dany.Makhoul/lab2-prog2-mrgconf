//Creator:Dany Makhoul
//Student id:1811257
import java.util.Scanner;
public class Bicycle{
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
    public String getManufacturer(){
        return this.manufacturer;
    }
    public int getNumberGears(){
                return this.numberGears;
    }
    public double getMaxSpeed(){
        return this.maxSpeed;
    }
    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer= manufacturer;
        this.numberGears= numberGears;
        this.maxSpeed = maxSpeed;
    }
    public String toString(){
        return "The Bicycle made by " + this.manufacturer + ",it has " + this.numberGears + " gears, And it speed up to " + this.maxSpeed + "km";

    }



}